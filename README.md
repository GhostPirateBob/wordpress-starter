# wordpress-starter

## Requirements

- node v10 - 12 and npm
- php composer v2 - [(install)](https://getcomposer.org/download/)
- xampp v7.2 - 7.4 [(install)](https://www.apachefriends.org/index.html)

## How to get started

1. Copy the contents of the "wordpress" folder into your htdocs/public_html (web root)
2. Import "wp_starter.sql" into a database called "wp_starter" using [XAMPP phpmyadmin](http://localhost/phpmyadmin)
3. Enter the sql details below into wp-config.php
4. cd into "wp-content/plugins/gpb-composer"
5. run ```composer install```
6. run ```composer dump-autoload```
7. cd into "wp-content/themes/understrap"
8. run ```npm install```
9. run ```npm run start``` to start "dev" scss watch
10. run ```npm run build``` to compile "dist" css

## mysql db details (see wp-config.php)

dn_name: wp_starter

db_user: root

db_password: (blank)

## wp-admin login details

user: admin

password: rmsN1lBhEw0SlYIidZjN

url: http://localhost/wp-admin
