<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_starter' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{3-|XWf~/>s{$E<S7JiKF+g9qO^W}5%Es*!oZ&lDr9b$eoiNUs]Jn/l&<Id <V F');
define('SECURE_AUTH_KEY',  'Ov9Bgz.>.VV$>Ja|(P:D~P%KY->?x}W-uh]Dax+L=:Q(@@O-|{[Alek]F6XBN%&|');
define('LOGGED_IN_KEY',    '%mVr,|,wJ03w!v^f+y+U7`q;:^e @`r/c>9zV<-.[d%&1/lxPNv,VuGGCTKy+{cx');
define('NONCE_KEY',        ',]W[2tyNYooPB%VEL1-+Te?stVy-z,lv5;1?ZS`1Z+m3u]&h}k ,K7Xw[g!+7AG-');
define('AUTH_SALT',        'tK)_73r}IJ`e|yyDd{LQdIw)JMm4{WzZ#RCto.MBw9-9X;@ 3x,FO-aM1Hv9YM7(');
define('SECURE_AUTH_SALT', 'i#psRl.Bz`V6s_x[bv-:UpDz13687r~jvi!|}`bCeLT.z%7c&hXH9xhuo ~-UBn>');
define('LOGGED_IN_SALT',   '}b^)iVMr~^DPQWKbCga TWFY5Hee+-.>i8es0qSuv6*8/,tVd=7A68Rlh+|)2>?h');
define('NONCE_SALT',       'lJl)-blo5H&=O3%b0jR*.|^z.F~/JrdFLoMWYPV3)3o|6P|Gr(X+u/FqL#wTUk4H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
