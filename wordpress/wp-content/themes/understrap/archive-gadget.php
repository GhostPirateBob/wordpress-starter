<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php	if (have_posts()) { ?>
	<div class="row">
	<?php while (have_posts()) { the_post(); ?>
		<div class="col-12 mb-3 d-flex flex-column">
			<h3 class="text-primary mb-3"><?php the_title(); ?></h3>
			<?php the_post_thumbnail('medium', ['class' => 'mb-3']); ?>
			<p><a class="btn btn-secondary" href="<?php the_permalink(); ?>">Read More</a><p>
		</div>
	</div>
	<?php } the_posts_pagination();
} ?>

<?php get_footer(); ?>
