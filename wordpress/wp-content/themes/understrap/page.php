<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
?>

<div class="wrapper" id="page-wrapper">
	<div class="container-fluid" id="content" tabindex="-1">
		<div class="row">
			<div class="col-12 content-area" id="primary">
				<main class="site-main" id="main">
						<?php while ( have_posts() ) : the_post(); ?>
						<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
							<div class="container">
								<div class="row">
									<div class="col-12">

										<header class="entry-header mb-5">
											<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
										</header><!-- .entry-header -->

										<div class="entry-content">
											<?php the_content(); ?>
										</div><!-- .entry-content -->

									</div><!-- .col-12 -->
								</div><!-- .row -->
							</div><!-- .container -->
						</article>
						<?php endwhile; // end of the loop. ?>
					</main><!-- #main -->
					</div>
				</div>
			</div><!-- #closing the primary container from left-sidebar-check.php -->
		</div><!-- .row -->
	</div><!-- #content -->
</div><!-- #page-wrapper -->

<?php get_footer(); ?>
