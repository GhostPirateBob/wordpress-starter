<?php
/**
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
?>

<div class="wrapper" id="page-wrapper">
	<div class="container-fluid" id="content" tabindex="-1">
		<div class="row">
			<div class="col-12 content-area" id="primary">
				<main class="site-main" id="main">
						<?php while ( have_posts() ) : the_post(); ?>
						<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
							<div class="container">
								<div class="row">
									<div class="col-12">

										<header class="entry-header mb-5">
											<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
										</header><!-- .entry-header -->

										<div class="entry-content">
											<div class="row">
												<div class="col-12 col-lg-6">
													<h3 class="text-primary text-bold text-center mb-0"><pre class="mb-0">display: <b>flex;</b> &lt;ul&gt;&lt;li&gt; <b>list-group</b></pre></h3>
													<ul class="list-group d-flex mb-5">
															<li class="list-group-item mb-3 mt-5 pt-1 list-group-item-primary">A simple primary list group item - <b>mb-3 mt-5 pt-1</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-secondary">A simple secondary list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-success">A simple success list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-danger">A simple danger list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-warning">A simple warning list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-info">A simple info list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-light">A simple light list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-5 mt-3 pb-5 list-group-item-dark">A simple dark list group item - <b>mb-5 mt-3 pb-5</b></li>
													</ul>
												</div>
												<div class="col-12 col-lg-6">
													<h3 class="text-secondary text-bold text-center mb-0"><pre class="mb-0">display: <b>block;</b> &lt;ul&gt;&lt;li&gt; <b>list-group</b></pre></h3>
													<ul class="list-group d-block mb-5">
															<li class="list-group-item mb-3 mt-5 pt-1 list-group-item-primary">A simple primary list group item - <b>mb-3 mt-5 pt-1</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-secondary">A simple secondary list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-success">A simple success list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-danger">A simple danger list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-warning">A simple warning list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-info">A simple info list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-3 mt-3 list-group-item-light">A simple light list group item - <b>mb-3 mt-3</b></li>
															<li class="list-group-item mb-5 mt-3 pb-5 list-group-item-dark">A simple dark list group item - <b>mb-5 mt-3 pb-5</b></li>
													</ul>
												</div>
											</div>
										</div><!-- .entry-content -->

									</div><!-- .col-12 -->
								</div><!-- .row -->
							</div><!-- .container -->
						</article>
						<?php endwhile; // end of the loop. ?>
					</main><!-- #main -->
					</div>
				</div>
			</div><!-- #closing the primary container from /global-templates/left-sidebar-check.php -->
		</div><!-- .row -->
	</div><!-- #content -->
</div><!-- #page-wrapper -->

<?php get_footer(); ?>
