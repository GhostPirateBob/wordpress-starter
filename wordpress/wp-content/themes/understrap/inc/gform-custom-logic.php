<?php

function gforms_confirmation_logic( $confirmation, $form, $entry, $ajax ) {
	if ( $entry[3] === 'flail' ) {
		$confirmation .= '<h3 class="text-center text-bold mb-3"> Correct! ';
		$confirmation .= '<span class="text-success">&#10004;</span> Flail wins &#128526; </h3>';
		$confirmation .= '<img alt="winner!" style="max-width: 543px;" src="';
		$confirmation .= get_stylesheet_directory_uri();
		$confirmation .= '/img/flail.jpg" class="d-block w-100 mb-4 mx-auto"/>';
	} else {
		$confirmation .= '<h3 class="text-center text-bold mb-3">Incorrect! ';
		$confirmation .= '<span class="text-danger">✘</span> Maybe <a href="';
		$confirmation .= get_site_url();
		$confirmation .= '" style="color: #2196F3!important;">try again</a>';
		$confirmation .= '&#128546; </h3><img alt="this is militari!" style="max-width: 543px;" src="';
		$confirmation .= get_stylesheet_directory_uri();
		$confirmation .= '/img/drm.jpg" class="d-block w-100 mb-4 mx-auto"/>';
	}
	return $confirmation;
}

add_filter( 'gform_confirmation', 'gforms_confirmation_logic', 10, 4 );
