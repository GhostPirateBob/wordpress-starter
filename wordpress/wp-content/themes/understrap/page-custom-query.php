<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
?>

<div class="wrapper" id="page-wrapper">
	<div class="container-fluid" id="content" tabindex="-1">
		<div class="row">
			<div class="col-12 content-area" id="primary">
				<main class="site-main" id="main">
						<?php while ( have_posts() ) : the_post(); ?>
						<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
							<div class="container">
								<div class="row">
									<div class="col-12">

										<header class="entry-header mb-5">
											<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
										</header><!-- .entry-header -->

										<div class="entry-contenta">
											<?php the_content(); ?>
										</div><!-- .entry-content -->
										<div class="row">
<?php 
// Query "gadget" custom post type and prepare fields
$query = new WP_Query(array(
		'post_type' => 'gadget',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		'posts_per_page' => 20 // -1 for infinite results
));

while ($query->have_posts()) {
	$query->the_post();
	$gadget_id = get_the_ID();
	$gadget_title = get_the_title();
	$gadget_content = apply_filters( 'the_content', get_the_content() );
	$gadget_permalink = get_the_permalink(); 
?>

											<div class="col-12 col-lg-6 mb-4" id="<?php echo "gadget-" . $gadget_id; ?>">
												<h2 class="mb-3"><?php echo $gadget_title; ?></h2>
												<?php the_post_thumbnail( 'large' ); ?>
												<div class="d-block w-100 mb-2">
													<?php echo wp_trim_words( $gadget_content, 30, '&mldr;' ); ?>
												</div>
												<p class="mb-0">
													<a class="btn btn-secondary" href="<?php echo $gadget_permalink; ?>"> Read Stuff&excl; </a>
												</p>
											</div>

<?php } wp_reset_query(); ?>
										</div>
									</div><!-- .col-12 -->
								</div><!-- .row -->
							</div><!-- .container -->
						</article>
						<?php endwhile; // end of the loop. ?>
					</main><!-- #main -->
					</div>
				</div>
			</div><!-- #closing the primary container from left-sidebar-check.php -->
		</div><!-- .row -->
	</div><!-- #content -->
</div><!-- #page-wrapper -->

<?php get_footer(); ?>
