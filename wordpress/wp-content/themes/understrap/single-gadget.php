<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-wrapper">
	<div class="container" id="content" tabindex="-1">
		<div class="row">
			<div class="col-md content-area" id="primary">
			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>
				<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

					<header class="entry-header mb-5">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						<div class="entry-meta">
							<?php understrap_posted_on(); ?>
						</div><!-- .entry-meta -->
					</header><!-- .entry-header -->

					<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

					<div class="entry-content">

						<?php the_content();
							echo '<br/>'; ?>

						<?php the_field( 'description' ); 
							echo '<br/><br/>'; ?>

						<?php // choose_your_weapon ( value )
						$choose_your_weapon_array = get_field( 'choose_your_weapon' );
						if ( $choose_your_weapon_array ):
							foreach ( $choose_your_weapon_array as $choose_your_weapon_item ):
								echo $choose_your_weapon_item;
								echo '<br/><br/>';
							endforeach;
							echo '<br/>';
						endif; ?>

						<?php if ( get_field( 'can_you_dig_it' ) == 1 ) { 
							echo 'true <br/><br/>'; 
						} else { 
							echo 'false <br/><br/>'; 
						} ?>

					</div><!-- .entry-content -->

				</article><!-- #post-## -->
				<?php understrap_post_nav(); ?>
				<?php endwhile; // end of the loop. ?>
			</main><!-- #main -->
			</div><!-- #closing the primary container from /global-templates/left-sidebar-check.php -->
		</div><!-- .row -->
	</div><!-- #content -->
</div><!-- #single-wrapper -->

<?php get_footer(); ?>
