<?php
/**
 * The template for the Home page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
?>

<div class="wrapper" id="page-wrapper">
	<div class="container-fluid" id="content" tabindex="-1">
		<div class="row">
			<div class="col-12 content-area" id="primary">
				<main class="site-main" id="main">
						<?php while ( have_posts() ) : the_post(); ?>
						<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
							<div class="container">
								<div class="row">
									<div class="col-12">

										<header class="entry-header mb-5">
											<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
										</header><!-- .entry-header -->
										<br/>
										<br/>
										<div class="entry-content">
											<?php the_content(); ?>
											<br/>
											<br/>
											<?php if ( class_exists( 'GFForms' ) ) {
												$form_id = '1';
												$form = GFAPI::get_form( $form_id );
												if ( !empty($form) && $form !== false ) {
													gravity_form($form_id, true, true, false, null, true, ($form_id * 100), true);
												}
											} ?>
										</div><!-- .entry-content -->

									</div><!-- .col-12 -->
								</div><!-- .row -->
							</div><!-- .container -->
						</article>
						<?php endwhile; // end of the loop. ?>
					</main><!-- #main -->
					</div>
				</div>
			</div><!-- #closing the primary container from /global-templates/left-sidebar-check.php -->
		</div><!-- .row -->
	</div><!-- #content -->
</div><!-- #page-wrapper -->

<?php get_footer(); ?>

