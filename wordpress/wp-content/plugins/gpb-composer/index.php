<?php
/**
 * Plugin Name: GPB Composer
 * Description: Composer Autoload Plugin
 * Version: 1.0.1
 * Author: GhostPirate Bob
 * License: MIT
 */

 // If this file is called directly, abort.
if (!defined( 'WPINC' ) || !function_exists('get_stylesheet_directory') || !function_exists('plugin_dir_path')) {
	exit();
}

define( 'GPB_COMPOSER', '1.0.1' );
$themeDir = get_stylesheet_directory();
$pluginDir = plugin_dir_path(__FILE__ );
$themeDir = str_replace('\\', '/', $themeDir);
$themeDir = str_replace('//', '/', $themeDir);
$pluginDir = str_replace('\\', '/', $pluginDir);
$pluginDir = str_replace('//', '/', $pluginDir);
$composerVendorPath = $pluginDir . '/vendor';
$composerVendorPath = str_replace('//', '/', $composerVendorPath);
$composerAutoloadTheme = $themeDir . '/vendor/autoload.php';
$composerAutoloadTheme = str_replace('//', '/', $composerAutoloadTheme);
$composerAutoloadPlugin = $pluginDir . '/vendor/autoload.php';
$composerAutoloadPlugin = str_replace('//', '/', $composerAutoloadPlugin);

function gpb_composer_error_notice() {
	?><div class="error notice">
		<p>Could not find autoload.php in the theme/vendor folder or plugin/vendor folder</p>
	</div><?php
	}

/**
 * The code that runs during plugin activation.
 */
function activate_gpb_composer() {
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_gpb_composer() {

}

register_activation_hook( __FILE__, 'activate_gpb_composer' );
register_deactivation_hook( __FILE__, 'deactivate_gpb_composer' );

// if ( class_exists( 'GFForms' ) && function_exists('gravity_form') ) {
// Apply Bootstrap 4 custom checkboxes and radio buttons to Gravity Fields generated forms
// Derived from https://jayhoffmann.com/using-gravity-forms-bootstrap-styles/
// }

add_action('admin_head', 'gpb_custom_admin_css');

function gpb_custom_admin_css() {
	?><style type="text/css">
	.smush-notice,
	#ame-plugin-menu-notice,
	.frash-notice,
	#welcome-panel,
	#footer-thankyou,
	#wp-admin-bar-comments,
	#wp-admin-bar-new-content,
	#wp-admin-bar-analytify,
	.wp-admin .pro-feature-wrapper,
	.wp-admin .wp-analytify-premium,
	.wp-admin #wp-analytify-help-tab,
	#wp-admin-bar-su-menu,
	#wp-admin-bar-wp-logo,
	#wp-admin-bar-updates,
	#contextual-help-link,
	img.menu_pto,
	#add_gform,
	.update-plugins,
	.update-nag,
	.wdspromos,
	#plugins-by-dreihochzwo,
	#plugins-by-john-huebner,
	#wpfooter,
	.about-wrap .extranotes,
	#cpac #ac-pro-version,
	#cpac #direct-feedback,
	#cpac #plugin-support,
	#cpto #cpt_info_box,
	#ws_sidebar_pro_ad,
	#ws-pro-version-notice,
	#ws_ame_how_to_box,
	#bsr-sidebar-wrap,.bsr-action-form>p,
	.wrap.ame-is-free-version .updated,
	.gadwp #postbox-container-1 {
		display: none !important;
	}
	.wp-admin #adminmenu .separator {
		height: 1px;
		padding: 0;
		background: rgba(255, 255, 255, 0.2);
	}
	#adminmenu #menu-dashboard {
		border-top: 1px solid rgba(255, 255, 255, 0.2);
	}
</style><?php
}

if ( is_dir($composerVendorPath) && file_exists($composerAutoloadPlugin) ) {
	require_once($composerAutoloadPlugin);
}

/**
 * The code that initiates composer modules globally.
 */
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
